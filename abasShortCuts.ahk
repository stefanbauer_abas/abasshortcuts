#singleinstance force
#NoEnv
#MaxHotkeysPerInterval 99000000
#HotkeyInterval 99000000
#KeyHistory 0
ListLines Off
Process, Priority, , A
SetBatchLines, -1
SetKeyDelay, -1, -1
SetMouseDelay, -1
SetDefaultMouseSpeed, 0
SetWinDelay, -1
SetControlDelay, -1
SendMode Input
SetTitleMatchMode, 2
DEBUG := 0

GroupAdd, abasgroup, ahk_exe wineks.exe
GroupAdd, abasgroup, ahk_exe abasgui.exe

if FileExist(A_MyDocuments . "\wineks.ico") 
{
Menu Tray, Icon, %A_MyDocuments%\wineks.ico
}

; neue Menue-Optionen hier hinzufuegen - Stelle 1 von 3
Gui, Add, ListBox, vShortCutListBox gShortCutListBox w227 r20, Direktmodus||Script bearbeiten|grep|alle FO der Maske|Infosysteme|Ticketsytem|FOP.txt|fop.txt gefiltert|Bildschirmauskunft|Servicearbeiten|Vartab|Betriebsdaten|Passwortdatensatz|Artikel|Aufzaehlung

Gui, Add, Button, Hidden Default, OK
Gui Show, w257 h290, ShortCuts
Gui +Resize
Gui, +AlwaysOnTop
Gui, -MinimizeBox

ShortCutListBox:
if A_GuiEvent <> DoubleClick 
    Return
GoSub, MenuSelection
return

ButtonOK:
GuiControlGet, FocusedControl, FocusV
if FocusedControl <> ShortCutListBox
    return
GoSub, MenuSelection
return

; neue Menue-Optionen hier hinzufuegen - Stelle 2 von 3
MenuSelection:
GuiControlGet, ShortCutListBox
If (ShortCutListBox = "Script bearbeiten") 
  GoSub, Edit
Else If (ShortCutListBox = "Direktmodus") 
  GoSub, Direct
  Else If (ShortCutListBox = "grep") 
  GoSub, grep
  Else If (ShortCutListBox = "alle FO der Maske") 
  GoSub, allfo
  Else If (ShortCutListBox = "FOP.txt") 
  GoSub, foptxt
  Else If (ShortCutListBox = "fop.txt gefiltert") 
  GoSub, fopfilter
  Else If (ShortCutListBox = "Infosysteme") 
  GoSub, Infosysteme
  Else If (ShortCutListBox = "Bildschirmauskunft") 
  GoSub, Bildschirmauskunft
  Else If (ShortCutListBox = "Servicearbeiten") 
  GoSub, Servicearbeiten
  Else If (ShortCutListBox = "Vartab") 
  GoSub, Vartab
  Else If (ShortCutListBox = "Betriebsdaten") 
  GoSub, Betriebsdaten
  Else If (ShortCutListBox = "Ticketsytem") 
  GoSub, Ticketsytem
  Else If (ShortCutListBox = "Passwortdatensatz") 
  GoSub, Passwortdatensatz
  Else If (ShortCutListBox = "Artikel") 
  GoSub, Artikel
Else If (ShortCutListBox = "Aufzaehlung") 
  GoSub, Aufzaehlung
Return

; neue Menue-Optionen hier hinzufuegen - Stelle 3 von 3
Edit:
{
CallCommand("*<(Text)> 'VN' <(Edit)>")
}

Infosysteme:
{
CallCommand("*<(Infosystem)> <(Empty)>")
}

Servicearbeiten:
{
CallCommand("*(Service)")
}

Betriebsdaten:
{
CallCommand("*<(Company)> $,gruppe=10 <(View)>,(CompanyData)")
}

Passwortdatensatz:
{
CallCommand("*<(Company)> <(Empty)>, (Password)")
}

Aufzaehlung:
{
CallCommand("*<(Enumeration)> <(Empty)>")
}

Artikel:
{
CallCommand("<(Part)> (300,2,0) <(View)>")
}

Ticketsytem:
{
CallCommand("i ABAS.TICKETSYSTEM")
}

Bildschirmauskunft:
{
CallCommand("i DISPLAYS")
}

; g als Menue-Hotkey fuer grep 
~g::
GoSub, grep
return

Direct:
{
    Gui, Destroy
    WinActivate ahk_group abasgroup
    SendInput {tab}
    Sleep 100
    SendInput {AppsKey} 
    Sendinput f
    Sendinput f
    Sendinput f
    Sendinput {enter}
    WinWait, FO-Direktmodus, , 2
    WinActivate ahk_group abasgroup
    
    if ErrorLevel
    {
        WindowNotFound()
    }
    else
    {
        Sleep 100
        SendInput '
        SendInput M
        SendInput |
        SendInput '
        Sendinput {end}
        Sendinput {left}
        ResetCursor()
        ExitApp
        return
    }
}

allfo:
{
    Gui, Destroy
    WinActivate ahk_group abasgroup
    SendInput {tab}
    Sleep 100
    SendInput {AppsKey} 
    Sendinput f
    Sendinput a
    Sendinput {enter}
    ExitApp
    return
}


Vartab:
{
    Gui, Destroy
    WinActivate
    IfWinActive, abas ERP Kommando
    {
        CallCommand("*<(Company)> <(Empty)>, (Vartab)") 
        ExitApp
        return
    }
    else
    {
        Gui, Destroy
        WinActivate ahk_group abasgroup
        SendInput {tab}
        Sleep 100
        SendInput {AppsKey} 
        Sendinput g
    }
    ExitApp
    return
}


grep:
{
    Gui, Destroy
    WinActivate ahk_group abasgroup
    Paste("!s (){ find . -maxdepth 2 -path ""./ow*"" -type f -print0 | xargs -0 grep -FIin ""${1:-$HOME}"" | sed 's/:/\t/' | sed 's/:/\t/' | column -s $'\t' -t `; } `; s """" ", 0 )
    ResizeWin(770)
	Sendinput {end}{Left}
    ExitApp
    return
}
    

foptxt:
{
    Gui, Destroy
    WinActivate ahk_group abasgroup
    Paste("*<(Text)> <(View)>", 1)
    WinWait, FO-Direktmodus, , 2
    if ErrorLevel
    {
        WindowNotFound()
    }
    else
    {
        WinActivate FO-Direktmodus
        SendInput .edi fop.txt{enter}
        ExitApp
        return
    }
    ExitApp
    return
}

fopfilter:
{
CallCommand("!sed 's/^[[:space:]]*//' fop.txt | grep -P ""^[\d]+ "" | grep -vE ""(java|button)"" | sed 's/\[.\]//' | tr -s "" "" | cut -d' ' -f1,2,3,5,7 | sort -k1,1nr -k3,3 -k5,5 | column -t")
}

CallCommand(Command)
{
    Gui, Destroy
    WinActivate ahk_group abasgroup
    ;PostMessage 0x111, 143,,,
    Paste(Command, 1)
	ResetCursor()
    ExitApp
    return
}

ResizeWin(Width = 0,Height = 0)
{
  WinGetPos,X,Y,W,H,A
  If %Width% = 0
    Width := W

  If %Height% = 0
    Height := H

  WinMove,A,,%X%,%Y%,%Width%,%Height%
  return
}


Paste(text, Enter:=0)
{
    SendInput ^e
    ClipSaved := ClipboardAll
    Clipboard = %text%
    WinWait, Expertenmodus, , 2
    if ErrorLevel
    {
        WindowNotFound()
    }
    WinActivate , Expertenmodus
    SendInput ^a{del}
    if Enter = 1 
    {
        SendInput ^v{Enter}
    }
    else
    {
        SendInput ^v
    }
    sleep 150
    Clipboard := ClipSaved 
    return
}

ResetCursor()
{
    SendInput {AppsKey}
	Sleep 10
	SendInput {Esc}
}

WindowNotFound()
{
    if DEBUG <> 0
    {
        MsgBox, WinWait timed out.
    }
    ExitApp
    return
}

GuiClose:
GuiEscape:
ExitApp

SC027::
send {enter}
return